import re

unit_dict={
	'g':['gm','gms','gram','grams','g'],
	'mg':['mgs','mg'],
	'ml':['ml'],
	'oz':['oz','ounce','ozs'],
	'fl oz':['fl','fl oz'],
	'l':['l','litre']
}

def removeAscii(rawString):
	return ''.join([i if ord(i) < 128 or i=="." else ' ' for i in rawString])

def preprocessNorm(sizeList):
	sizeList=re.sub(r'(?<=\d)[,\.](?=\d)',".",sizeList)
	return ''.join(e if e.isalnum() or e=="." else ' ' for e in sizeList ).lower()

def xstr(s):
	'''Return '' if string is None'''
	return '' if s is None else str(s)

def removeNum(text):
	return ''.join([i if not i.isdigit() else ' ' for i in text])

def extractNum(text):
	numdict={}
	num=re.findall(r'\d+(?:[\.\,]\d+)?', text)
	count_length_del=0
	for itr in num:
		loc=text.find(itr)
		if loc!=-1:
			numdict[itr]=loc+count_length_del
			count_length_del+=len(str(itr))
			text=text.replace(itr,"")
	print "NumDict::: ",numdict
	return numdict


def extractSizeFromText(text):
	text=" "+text+" "
	text=removeAscii(xstr(text))
	text=preprocessNorm(text)
	text=" "+text+" "
	foundUnit={}
	for unit in unit_dict:
		for eachvariant in unit_dict[unit]:
			loc=removeNum(text).find(" "+eachvariant+" ")
			print eachvariant,loc
			if loc!=-1:
				foundUnit[eachvariant]=loc
	numdict=extractNum(text)
	pairs=[]
	for eachUnit in foundUnit:
		for num in numdict: 
			if foundUnit[eachUnit]> numdict[num]:
				numdict[num]=numdict[num]+len(str(num))-1
			if foundUnit[eachUnit]-numdict[num]<=3 and foundUnit[eachUnit]-numdict[num]>=0:
				pairs.append(num+" "+eachUnit)
	return pairs

import HTMLParser


def sizeCleanup(text):
	text=" "+text.lower()+" "
	size_quantity=['mg','milligram','milligrams','mgs',
	'g','gram','grams','gr',
	'kg','kilo','kilogram','kilograms','kgs',
	'ml','millilitre','milliliter','milliliters','millilitres','fl oz','fl ounces','fluid','fl',
	'l','litre','litres','oz','ounce','ozs','ounces'
	]
	#Basic preprocessing
	html_parser = HTMLParser.HTMLParser()
	text = html_parser.unescape(text)
	text=''.join(e if e.isalnum() or e=="." or e=="," else ' ' for e in text).lower()
	numbers = re.compile('\d+(?:[\.\,]\d+)?')
	extractedNumList=numbers.findall(text)
	for num in extractedNumList:
		if "," in num:
			new =num.replace(",",".")
			text=text.replace(num,new)
	text=text.replace(","," ")
	text=removeAscii(xstr(text))
	numbers = re.compile('\d+(?:[\.\,]\d+)?')
	extractedNumList=numbers.findall(text)
	for num in extractedNumList:
		for size in size_quantity:
			text=re.sub(num+'\s*'+size+"\s+"," ",text)
	text=' '.join(text.split())
	cleanText=text.strip().upper()
	if cleanText=="":
		cleanText="NA"
	return cleanText

def main():
	'''
	with open("Estee_Lauder_Delivery_0514.csv","r") as fin:
		for line in fin:
			values=line.split(",")
			# description= values[25]
			# title=values[5]
			try:
				color=values[17]
			except:
				continue
			# print description,title,color
			# print "title: ",extractSizeFromText(title)
			# print "color: ",extractSizeFromText(color)
			# print description
			# print "description: ",extractSizeFromText(description)
			print "Before: ",color
			print "cleaned up: ",cleanup(" "+color+" ")
	'''
	# x= "288 Road Movie (2 g)"
	x="12 oz/ 340 g"
	print x
	print extractSizeFromText(x)
	# print sizeCleanup(x)

if __name__ == '__main__':
	main()
