import json
import csv
import utils as utils
import re



def preprocessing(rawText,sizeInput="",removeComma=False):
	'''rawText is 1 value in String format'''
	normText=rawText.strip()
	normText=normText.lower()
	if normText=='na' or normText==None:
		normText=''
	if normText=='':
		return rawText,normText,[],[]
	normText=utils.removeAscii(normText)
	if removeComma==True:
		normText=re.sub(r'(?<=\d)[,\.](?=\d)',".",normText)
	normText=utils.preprocessNorm(normText)
	numbers=utils.extractNumbers(normText)
	unit=utils.extractUnit(normText)
	for i in unit:

		unit[i]['value']=utils.mapNumWithUnit(normText,unit[i]['found'],numbers)[1]
		typeQ,convValue,convSI=utils.convertSI(unit[i]['value'],i)
		unit[i]['Converted']={'type':typeQ,'convValue':convValue,'convSI':convSI}
		default=unit[unit.keys()[0]]
		typeInput,convInput,convSIInput=utils.reverseConvert(unit[i]['Converted'],sizeInput,default)
		unit[i]['Input']={'type':typeInput,'convValue':convInput,'convUnit':convSIInput}
	return rawText,normText,numbers,unit


def main():
	
	'''Input:- Pass Size Key
	Output:- raw:- Passed String, string which contains 1 quantity. 
	norm:- String after basic preprocessing
	num:- List of Numbers extracted from input string
	unit:- Units found, with their values and Converted to SI format'''
	# print "Packing: net weight 0.34 g (0.01 oz)"
	# print preprocessing("4ml","8.5 g",True)
	print preprocessing("12 gms")
	# print utils.unitExtract('45 ml')
if __name__ == '__main__':
	main()
