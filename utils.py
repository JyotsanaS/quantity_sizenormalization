import re
import HTMLParser

def convertHtmlTags(rawString):
	html_parser = HTMLParser.HTMLParser()
	rawString = html_parser.unescape(rawString)
	return rawString

def removeDecimal(rawString):
	rawString=rawString.strip()
	if ".0" in " "+rawString+" ":
		return rawString.replace(".0","").strip()
	else:
		return rawString.strip()

size_quantity=[
['mg','milligram','milligrams','mgs'],
['g','gram','grams','gr','gm','gms'],
['kg','kilo','kilogram','kilograms','kgs'],
['ml','millilitre','milliliter','milliliters','millilitres'],
['l','litre','litres'],['oz','ounce','ozs','ounces'],['fl oz','fl ounces','fluid','fl']
]

conversion={
	#Convert to SI units
	'length':{'mm':0.001,'m':1,'cm':0.01,'si':'m'},
	'volume':{'ml':0.001,'l':1,'kl':1000,'fl oz':0.0295735,'si':'l'},
	'weight':{'mg':0.001,'g':1,'kg':1000,'oz':28.3495,'si':'g'}
}

# size_conversion=[
# ]

def removeAscii(rawString):
	# rawString=rawString.encode('utf-8').strip()
	rawString=rawString.decode('utf-8').strip()
	# return rawString
	return ''.join([i if ord(i) < 128 else ' ' for i in rawString])

# def preprocessNorm(sizeList):
# 	normSizeList=[]
# 	for i in sizeList:
# 		normSizeList.append(re.sub('[^a-zA-Z0-9.]+', ' ', i).lower().strip())
# 		# normSizeList.append(' '.join(e for e in i if e.isalnum()).lower())
# 	return normSizeList

def preprocessNorm(rawText):
	'''Remove special characters'''
	z=re.sub("[(#{})\[\]\']", " ", rawText).strip()
	z=z.replace('\\'," ")
	z = " ".join([i.rstrip(".") for i in z.split()])
	# rawText=''.join(e for e in rawText if e.isalnum())

	return z

def xstr(s):
	'''Return '' if string is None'''
	return '' if s is None else str(s)


def extractNumbers(data):
	'''
	extracted=''.join([c for c in data if c in '1234567890.- ']).strip()
	extractedNumList=re.split('-|s ',extracted)
	extractedNumList=list(filter(lambda a: a != '', extractedNumList))
	'''
	numbers = re.compile('\d+(?:[\.\,]\d+)?')
	extractedNumList=numbers.findall(data)
	print "Extracted:::: ",extractedNumList
	for pos,num in enumerate(extractedNumList):
		if "," in num:
			new =num.replace(",",".")
			extractedNumList[pos]=new
	return extractedNumList

def mapNumWithUnit(rawText,unit,numList):
	rawText=rawText+" "
	unitResult=rawText.find(" "+unit+" ")
	nearest=5
	num_matched=None
	for i in numList:
		result=rawText.find(i)+len(str(i))-1
		start=rawText.find(i)
		diff=0
		# print "number:::end of num,start of num,unit",i,result,start,unitResult
		if (unitResult>result):
			diff=unitResult-result
			if diff <nearest:
				num_matched=i 
			nearest=diff
		else:
			diff=result-unitResult-len(unit)+1
			if diff<nearest:
				num_matched=i
			nearest=diff
	if num_matched!=None:
		num_matched=float(num_matched)

	return unit,num_matched

def extractUnit(data):
	found=[]
	data=" "+data+" "
	for quantityList in size_quantity:
		for i in quantityList:
			# if i+" " in " "+data+" " or " "+i in " "+data+" ":
			if i+" " in " "+data+" " or " "+i in " "+data+" ":
				if len(i)<=2:
					if re.search(r'\d+\s*'+i+" "," "+data+" "):
						found.append(i)
				else:
					print "here4"
					found.append(i)
	new=[]
	new_dict_unnorm={}
	for num,i in enumerate(found):
		flag=True
		for num2,j in enumerate(found):
			if i in j and len(j)>len(i) and num!=num2:
				flag=False
				break
		if flag==True:
			new.append(i)

	for itr in size_quantity:
		for i in new:
			if i in itr:
				new_dict_unnorm[itr[0]]={'found':i,'unnorm':itr}
	return new_dict_unnorm


def convertSI(value,unit):
	Type=None
	convValue=None
	convUnit=None
	if value==None or unit==None:
		return Type,convValue,convUnit
	# print "Here: ",value,unit
	for key in conversion:
		NormUnit=conversion[key].keys()
		for itr in NormUnit:
			# print "Here1",itr
			if unit==itr:
				# print "Here2"
				multiplier=conversion[key][itr]
				convValue=multiplier*value
				convUnit=conversion[key]['si']
				Type=key
				break
	return Type,convValue,convUnit

def unitExtract(data):
	found=[]
	data=" "+data+" "
	data=preprocessNorm(data)
	data=''.join(e for e in data if e.isalpha() or e==" ")
	print data
	for quantityList in size_quantity:
		for i in quantityList:
			# print "Here 5",i
			if " "+i+" " in " "+data+" ":
				# print "Here 55",i, data
				found.append(i)
	# print "Found: ",found
	new_dict_unnorm={}
	# for num,i in enumerate(found):
	# 	flag=True
	# 	for num2,j in enumerate(found):
	# 		# print i,j
	# 		if i in j and len(j)>len(i) and num!=num2:
	# 			flag=False
	# 			break
	# 	if flag==True:
	# 		new.append(i)
	for i in found: 
		for itr in size_quantity:
			if i in itr:
				new_dict_unnorm[itr[0]]={'found':i,'unnorm':itr}
	return new_dict_unnorm

def reverseConvert(SIunit,unit,default=None):
	print "SIUNIT::::",SIunit
	print "unit::::",unit
	print "default:::",default
	value=SIunit['convValue']
	si=SIunit['convSI']
	unittype=SIunit['type']
	try:
		try:
			targetunit=unitExtract(unit).keys()[0]
			print "Target:::",targetunit
		except:
			return unittype,value,si
		if targetunit==None or targetunit=="" or unit=="":
			return unittype,value,si
		conversionRatio=conversion[unittype][targetunit]
		convValue=value/conversionRatio
	except:
		# return reverseConvert(SIunit,default,default)
		return unittype,value,si
	return unittype,convValue,targetunit